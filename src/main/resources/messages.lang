# ezTickets Message Configuration #
prefix='&6[&bezTickets&6] '

# NOTE: You can set most of these as '' to cancel the message
# Ex: command.ticket.help=''

# {0} is the sender of the message
# {1} is the message that was sent
chat_message_format=&b{0}: &6{1}

# {0} is the page selected in the list
# {1} is the total number of pages in the list
list_message_format=&6Page &b{0} &6of &b{1}

# {0} is the month MM
# {1} is the day dd
# {2} is the year yyyy
# {3} is the hour hh
# {4} is the minute mm
# {5} is the local timezone of the hosting server
time_format={0}/{1}/{2} {3}:{4} {5}

command.no_permission={prefix}&cYou do not have permission to execute this command.
command.no_sub={prefix}&cSub command not found. Do /ticket for help.
command.player_only={prefix}You must be a player to execute this command.

command.invalid_id={prefix}&cYou have not specified a valid ticket ID.
command.ticket_not_found={prefix}&cNo ticket exists by that ID.
command.player_not_found={prefix}&cPlayer not found.
command.ticket_not_assigned={prefix}&cThat ticket is not assigned to you.
command.specify_player={prefix}&cPlease specify a player.

command.ticket.help=&6== &bezTickets &6==

# {0} is the ticket ID
command.ticket.adddetail.success={prefix}&aSuccessfully added detail to ticket {0}.
command.ticket.adddetail.help=&b/ticket adddetail [ticket-id] [detail] : &6Add to the detail of a ticket

# {0} is the ticket id
# {1} is the person that the ticket was assigned to
command.ticket.assign.success={prefix}&aSuccessfully assigned ticket {0} to {1}.
command.ticket.assign.help=&b/ticket assign [player] [ticket-id] : &6Assign a ticket to another player

command.ticket.claim.already_assigned={prefix}&cThat ticket has already been claimed.
command.ticket.claim.already_closed={prefix}&cThat ticket has already been closed.
# {0} is the ticket ID
command.ticket.claim.success={prefix}&aSuccessfully claimed ticket {0}.
command.ticket.claim.help=&b/ticket claim [ticket-id] : &6Claim a ticket

command.ticket.close.already_closed={prefix}&cThat ticket has already been closed.
# {0} is the ticket ID
command.ticket.close.success={prefix}&aSuccessfully closed ticket {0}.
command.ticket.close.help=&b/ticket close [ticket-id] : &6Close a ticket

command.ticket.create.max_already_reached={prefix}&cYou have reached the maximum amount of open tickets!
# {0} is the ticket ID
command.ticket.create.success={prefix}&aSuccessfully opened ticket with ID {0}.
command.ticket.create.help=&b/ticket create [detail] : &6Create a new ticket

# {0} is the ticket ID
# {1} is the submitter
# {2} is the ticket status
# {3} is the assignee or if none look to command.ticket.info.message.not_assigned
# {4} is the time it was submitted, ex: 7/30/2017 21:32 EST
# {5} is the location in which the ticket was submitted, format: world, x, y, z
# {6} is the ticket detail (may be multiple lines if extra detail was added)
# Info: the \ at the end of the message must stay. This is to allow you to create new lines in the file for the same
# message. The \n makes a new line in the player's chat. \n\ = new line in the player's chat + this file
command.ticket.info.message=\
  &bTicket &6{0} &bSubmitted by: &6{1}\n\
  &bStatus: &6{2}\n\
  &bAssigned to: &6{3}\n\
  &bTime Submitted: &6{4}\n\
  &bLocation: &6{5}\n\
  &bDetail:\n\
  &c{6}
command.ticket.info.message.not_assigned=Not assigned
command.ticket.info.not_yours={prefix}&cThis is not your ticket.
command.ticket.info.help=&b/ticket info [ticket-id] : &6Get information about a ticket

# {0} is the type of list (Open/Claimed/Closed/My/[player]'s) note: editable in the following properties
command.ticket.list.header=&d{0} &bTickets : &6Ticket ID &b| &6Submitter &b| &6Date Submitted &b| &6Status
command.ticket.list.header.open=&aOpen
command.ticket.list.header.assigned=&eAssigned
command.ticket.list.header.closed=&cClosed
command.ticket.list.header.all=All
command.ticket.list.header.mine=My
# {0} is the player who holds the following tickets
# Two '' means one ' in-game
command.ticket.list.header.player={0}''s

# {0} is the ticket ID
# {1} is the submitter
# {2} is the date submitted
# {3} is the ticket status
# {4} is the detail
command.ticket.list.entry=&6{0} &b| &6{1} &b| &6{2} &b| &e{3}

command.ticket.list.no_permission={prefix}&cYou do not have permission to view these tickets!
command.ticket.list.none={prefix}&cNo tickets found.
# {0} is the amount of pages
command.ticket.list.max={prefix}&cThere are only {0} pages.
command.ticket.list.help=&b/ticket list [open/assigned/closed/all/mine/player [player]] [page#] : &6List a certain \
  type of tickets.

command.ticket.messages.none={prefix}&cNo recent messages logged for this ticket.
# {0} is the amount of pages in the list
command.ticket.messages.max={prefix}&cThere are only {0} pages.
command.ticket.messages.help=&b/ticket messages [ticket-id] [all / page#] : &6See the recent messages for a ticket

command.ticket.reload.success={prefix}&aSuccessfully reloaded configuration and messages!
command.ticket.reload.help=&b/ticket reload : &6Reload configuration and messages

# {0} is the ticket ID
command.ticket.remove.success={prefix}&aSuccessfully removed ticket {0}.
command.ticket.remove.help=&b/ticket remove [ticket-id] : &6Remove a ticket.

# {0} is the player
# {1} is the amount of open tickets
# {2} is the amount of assigned tickets
# {3} is the amount of closed tickets
command.ticket.total.message={prefix}&a{0} has {1} open, {2} assigned, and {3} closed ticket(s).
# {0} is the amount of open tickets
# {1} is the amount of assigned tickets
# {2} is the amount of closed tickets
command.ticket.total.message.you={prefix}&aYou have {0} open, {1} assigned, and {2} closed ticket(s).
command.ticket.total.help=&b/ticket total [player] : &6Get the total tickets for a player / yourself.

command.ticket.tp.invalid={prefix}&cError: World not loaded.
# {0} is the ticket ID
command.ticket.tp.success={prefix}&aTeleported to ticket {0}.
command.ticket.tp.help=&b/ticket tp [ticket-id] : &6Teleport to a ticket's location.

# {0} is the total amount of open tickets (message is only sent if that number is greater than 0
notify.join.moderator.open_tickets={prefix}&aThere are currently {0} open ticket(s).
# {0} is the total amount of tickets assigned to this player (message is only sent if that number is greater than 0
notify.join.moderator.assigned_tickets={prefix}&aYou currently have {0} ticket(s) assigned to you.

# {0} is the amount of tickets that this player has submitted which are open
notify.join.player={prefix}You have {0} open ticket(s).

# {0} is the person who assigned the ticket to a player
# {1} is the ticket ID
# {2} is the person that the ticket was assigned to
notify.assign.moderators={prefix}&a{0} has assigned ticket {1} to {2}

# {0} is the ticket ID
# {1} is the person who assigned the ticket to this player
notify.assign.assignee={prefix}&aYou have been assigned to ticket {0} by {1}.

# {0} is the ticket ID
# {1} is the assignee of the ticket
notify.assign.player={prefix}&aYour ticket (ID {0}) has been claimed.

# {0} is the claimer of the ticket
# {1} is the ticket ID
notify.claim.moderators={prefix}&a{0} has claimed ticket {1}.

# {0} is the ticket ID
# {1} is the assignee of the ticket
notify.claim.player={prefix}&aYour ticket (ID {0}) has been claimed.

# {0} is the closer of the ticket
# {1} is the ticket ID
notify.close.moderators={prefix}&a{0} has closed ticket {1}.

# {0} is the ticket ID
# {1} is the closer of the ticket
notify.close.player={prefix}&aYour ticket (ID {0}) has been closed.

# {0} is the submitter of the ticket
# {1} is the ticket ID
notify.create.moderators={prefix}&a{0} has opened a ticket with ID {1}.
