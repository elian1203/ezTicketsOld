package net.urbanmc.eztickets.manager;

import org.apache.commons.io.IOUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.io.*;
import java.text.MessageFormat;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class Messages {

	private static Messages instance = new Messages();

	private final File FILE = new File("plugins/ezTickets", "messages.lang");

	private ResourceBundle bundle;

	private Messages() {
		createFile();
		loadBundle();
	}

	public static Messages getInstance() {
		return instance;
	}

	public static String getString(String key, Object... args) {
		return instance.getStringFromBundle(key, args);
	}

	@SuppressWarnings("ResultOfMethodCallIgnored")
	private void createFile() {
		if (!FILE.getParentFile().isDirectory()) {
			FILE.getParentFile().mkdir();
		}

		if (!FILE.exists()) {
			try {
				FILE.createNewFile();

				InputStream input = getClass().getClassLoader().getResourceAsStream("messages.lang");
				OutputStream output = new FileOutputStream(FILE);

				IOUtils.copy(input, output);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void loadBundle() {
		try {
			InputStream input = new FileInputStream(FILE);
			Reader reader = new InputStreamReader(input, "UTF-8");

			bundle = new PropertyResourceBundle(reader);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String getStringFromBundle(String key, Object... args) {
		try {
			return format(bundle.getString(key), args);
		} catch (Exception e) {
			Bukkit.getLogger().severe("[ezTickets] Missing message in messages.lang! Message key: " + key);
			return key;
		}
	}

	private String format(String message, Object... args) {
		message = message.replace("{prefix}", bundle.getString("prefix"));

		if (args != null) {
			message = MessageFormat.format(message, args);
		}

		message = ChatColor.translateAlternateColorCodes('&', message);

		return message;
	}

	public void reload() {
		createFile();
		loadBundle();
	}
}
