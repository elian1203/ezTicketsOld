package net.urbanmc.eztickets.manager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.urbanmc.eztickets.gson.TicketList;
import net.urbanmc.eztickets.gson.TicketSerializer;
import net.urbanmc.eztickets.object.ChatMessage;
import net.urbanmc.eztickets.object.ChatMessageList;
import net.urbanmc.eztickets.object.Ticket;
import net.urbanmc.eztickets.object.Ticket.TicketStatus;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.stream.Collectors;

public class TicketManager {

	private static TicketManager instance = new TicketManager();

	private final File FILE = new File("plugins/ezTickets", "tickets.json");
	private final Gson gson =
			new GsonBuilder().registerTypeAdapter(Ticket.class, new TicketSerializer()).setPrettyPrinting().create();

	private List<Ticket> tickets;
	private ChatMessageList messages = new ChatMessageList();

	private TicketManager() {
		createFile();
		loadTickets();
	}

	public static TicketManager getInstance() {
		return instance;
	}

	@SuppressWarnings("ResultOfMethodCallIgnored")
	private void createFile() {
		if (!FILE.getParentFile().isDirectory()) {
			FILE.getParentFile().mkdir();
		}

		if (!FILE.exists()) {
			try {
				FILE.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void loadTickets() {
		try {
			String json = FileUtils.readFileToString(FILE);

			tickets = gson.fromJson(json, TicketList.class).getTickets();
		} catch (Exception ex) {
			if (!(ex instanceof NullPointerException)) {
				Bukkit.getLogger().log(Level.SEVERE, "[ezTickets] Error loading tickets!", ex);
			}

			tickets = new ArrayList<>();
		}
	}

	public void saveTickets() {
		try {
			FileUtils.write(FILE, gson.toJson(new TicketList(tickets)));
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public List<Ticket> getTickets() {
		return new ArrayList<>(tickets);
	}

	public void addTicket(Ticket ticket) {
		tickets.add(ticket);
		saveTickets();
	}

	public void removeTicket(Ticket ticket) {
		tickets.remove(ticket);
		saveTickets();
	}

	public synchronized int getNewTicketId() {
		if (tickets.isEmpty()) {
			return 1;
		} else {
			Ticket latestTicket = tickets.get(tickets.size() - 1);
			return latestTicket.getTicketId() + 1;
		}
	}

	public Ticket getTicketById(int ticketId) {
		for (Ticket ticket : tickets) {
			if (ticket.getTicketId() == ticketId)
				return ticket;
		}

		return null;
	}

	public List<Ticket> getTicketsForPlayer(UUID submitter) {
		return tickets.stream().filter(t -> t.getSubmitter().equals(submitter)).collect(Collectors.toList());
	}

	public long getOpenTicketsForPlayer(UUID submitter) {
		return getTicketsForPlayer(submitter).stream().filter(t -> t.getStatus() == TicketStatus.OPEN).count();
	}

	public void addChatMessage(ChatMessage message) {
		messages.add(message);
	}

	public List<ChatMessage> getRecentMessages() {
		return new ArrayList<>(messages);
	}

	public void reloadChatMessages() {
		messages.clear();
		ChatMessageList.updateMaxChatMessages();
	}
}
