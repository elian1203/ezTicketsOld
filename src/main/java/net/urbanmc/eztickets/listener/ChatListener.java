package net.urbanmc.eztickets.listener;

import net.urbanmc.eztickets.manager.ConfigManager;
import net.urbanmc.eztickets.manager.TicketManager;
import net.urbanmc.eztickets.object.ChatMessage;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener {

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPlayerChat(AsyncPlayerChatEvent e) {
		if (ConfigManager.getConfig().getInt("chat-messages-logged") <= 0)
			return;

		String name = e.getPlayer().getName(), message = e.getMessage();

		TicketManager.getInstance().addChatMessage(new ChatMessage(name, message));
	}
}
