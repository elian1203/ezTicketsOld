package net.urbanmc.eztickets.listener;

import net.urbanmc.eztickets.EzTickets;
import net.urbanmc.eztickets.manager.ConfigManager;
import net.urbanmc.eztickets.manager.Messages;
import net.urbanmc.eztickets.manager.TicketManager;
import net.urbanmc.eztickets.object.Permission;
import net.urbanmc.eztickets.object.Ticket.TicketStatus;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinListener implements Listener {

	private EzTickets plugin;

	public JoinListener(EzTickets plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();

		if (!p.hasPermission(Permission.JOIN_NOTIFY.getStringPermission()))
			return;

		boolean joinNotifyModerator = ConfigManager.getConfig().getBoolean("notify.join.moderator"), joinNotifyPlayer =
				ConfigManager.getConfig().getBoolean("notify.join.player"), hasJoinNotifyModeratorPermission =
				p.hasPermission(Permission.JOIN_NOTIFY_MODERATOR.getStringPermission());

		long openTicketsCount =
				TicketManager.getInstance().getTickets().stream().filter(t -> t.getStatus() == TicketStatus.OPEN)
						.count(), personalAssignedTicketsCount = TicketManager.getInstance().getTickets().stream()
				.filter(t -> t.getStatus() != TicketStatus.CLOSED && p.getUniqueId().equals(t.getAssignee())).count();


		if (joinNotifyModerator && hasJoinNotifyModeratorPermission) {
			if (openTicketsCount > 0) {
				sendMessage(p, "notify.join.moderator.open_tickets", openTicketsCount);
			}

			if (personalAssignedTicketsCount > 0) {
				sendMessage(p, "notify.join.moderator.assigned_tickets", personalAssignedTicketsCount);
			}
		}

		long personalOpenTicketsCount = TicketManager.getInstance().getOpenTicketsForPlayer(p.getUniqueId());

		if (joinNotifyPlayer && personalOpenTicketsCount > 0) {
			sendMessage(p, "notify.join.player", personalOpenTicketsCount);
		}
	}

	private void sendMessage(Player p, String property, Object... args) {
		String message = Messages.getString(property, args);

		Bukkit.getServer().getScheduler().runTaskLater(plugin, () -> p.sendMessage(message), 5);
	}
}
