package net.urbanmc.eztickets.gson;

import com.google.gson.*;
import net.urbanmc.eztickets.object.ChatMessage;
import net.urbanmc.eztickets.object.Ticket;
import net.urbanmc.eztickets.object.Ticket.TicketStatus;
import net.urbanmc.eztickets.object.TicketLocation;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TicketSerializer implements JsonSerializer<Ticket>, JsonDeserializer<Ticket> {

	@Override
	public JsonElement serialize(Ticket ticket, Type type, JsonSerializationContext context) {
		JsonObject obj = new JsonObject();
		Gson gson = new Gson();

		obj.addProperty("id", ticket.getTicketId());

		obj.addProperty("submitter", ticket.getSubmitter().toString());

		if (ticket.getStatus() == TicketStatus.ASSIGNED) {
			obj.addProperty("assignee", ticket.getAssignee().toString());
		}

		obj.addProperty("timeSubmitted", ticket.getTimeSubmitted());

		obj.add("location", gson.toJsonTree(ticket.getLocation()));

		obj.addProperty("detail", ticket.getDetail());

		JsonArray messages = new JsonArray();

		for (ChatMessage message : ticket.getRecentMessages()) {
			messages.add(gson.toJsonTree(message));
		}

		obj.add("messages", messages);

		obj.addProperty("status", ticket.getStatus().toString());

		return obj;
	}

	@Override
	public Ticket deserialize(JsonElement element, Type type,
	                          JsonDeserializationContext context) throws JsonParseException {
		JsonObject obj = element.getAsJsonObject();
		Gson gson = new Gson();

		int ticketId = obj.get("id").getAsInt();

		UUID submitter = UUID.fromString(obj.get("submitter").getAsString()), assignee = null;

		if (obj.has("assignee")) {
			assignee = UUID.fromString(obj.get("assignee").getAsString());
		}

		long timeSubmitted = obj.get("timeSubmitted").getAsLong();

		TicketLocation location = gson.fromJson(obj.get("location"), TicketLocation.class);

		String detail = obj.get("detail").getAsString();

		List<ChatMessage> messages = new ArrayList<>();

		for (JsonElement message : obj.getAsJsonArray("messages")) {
			messages.add(gson.fromJson(message, ChatMessage.class));
		}

		TicketStatus status = TicketStatus.valueOf(obj.get("status").getAsString());

		return new Ticket(ticketId, submitter, assignee, timeSubmitted, location, detail, messages, status);
	}
}
