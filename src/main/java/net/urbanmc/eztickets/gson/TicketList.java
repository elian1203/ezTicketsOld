package net.urbanmc.eztickets.gson;

import net.urbanmc.eztickets.object.Ticket;

import java.util.List;

public class TicketList {

	private List<Ticket> tickets;

	public TicketList(List<Ticket> tickets) {
		this.tickets = tickets;
	}

	public List<Ticket> getTickets() {
		return tickets;
	}
}
