package net.urbanmc.eztickets.command;

import net.urbanmc.eztickets.command.subs.*;
import net.urbanmc.eztickets.manager.Messages;
import net.urbanmc.eztickets.object.Permission;
import net.urbanmc.eztickets.object.SubCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TicketCommand implements CommandExecutor {

	private List<SubCommand> subs = new ArrayList<>();

	public TicketCommand() {
		loadSubCommands();
	}

	private void loadSubCommands() {
		subs.add(new AddDetail());
		subs.add(new Assign());
		subs.add(new Claim());
		subs.add(new Close());
		subs.add(new Create());
		subs.add(new Info());
		subs.add(new ListSub());
		subs.add(new net.urbanmc.eztickets.command.subs.Messages());
		subs.add(new Reload());
		subs.add(new Remove());
		subs.add(new Total());
		subs.add(new Tp());
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.hasPermission(Permission.COMMAND_BASE.getStringPermission())) {
			sendMessage(sender, "command.no_permission");
			return true;
		}

		if (args.length == 0) {
			return help(sender);
		}

		SubCommand sub = findSubCommand(args[0]);

		if (sub == null) {
			sendMessage(sender, "command.no_sub");
			return true;
		}

		if (!(sender instanceof Player) && sub.isPlayerOnly()) {
			sendMessage(sender, "command.player_only");
			return true;
		}

		if (!sender.hasPermission(sub.getPermission().getStringPermission())) {
			sendMessage(sender, "command.no_permission");
			return true;
		}

		sub.execute(sender, Arrays.copyOfRange(args, 1, args.length));

		return true;
	}

	private boolean help(CommandSender sender) {
		boolean player = sender instanceof Player;

		sendMessage(sender, "command.ticket.help");

		for (SubCommand sub : subs) {
			boolean canUse =
					player && sub.isPlayerOnly() || player && !sub.isPlayerOnly() || !(!player && sub.isPlayerOnly());

			if (sender.hasPermission(sub.getPermission().getStringPermission()) && canUse) {
				sendMessage(sender, sub.getHelpProperty());
			}
		}

		return true;
	}

	private SubCommand findSubCommand(String arg) {
		for (SubCommand sub : subs) {
			if (sub.getSubCommand().equalsIgnoreCase(arg) ||
					Arrays.stream(sub.getAliases()).anyMatch(a -> a.equalsIgnoreCase(arg))) {
				return sub;
			}
		}

		return null;
	}

	private void sendMessage(CommandSender sender, String property, Object... args) {
		String message = Messages.getString(property, args);

		if (sender instanceof Player) {
			sender.sendMessage(message);
		} else {
			sender.sendMessage(ChatColor.stripColor(message));
		}
	}
}
