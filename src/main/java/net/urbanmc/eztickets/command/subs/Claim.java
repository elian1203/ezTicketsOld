package net.urbanmc.eztickets.command.subs;

import net.urbanmc.eztickets.manager.ConfigManager;
import net.urbanmc.eztickets.manager.Messages;
import net.urbanmc.eztickets.object.Permission;
import net.urbanmc.eztickets.object.SubCommand;
import net.urbanmc.eztickets.object.Ticket;
import net.urbanmc.eztickets.object.Ticket.TicketStatus;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Claim extends SubCommand {


	public Claim() {
		super("claim", Permission.COMMAND_CLAIM, true, new String[0]);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (args.length == 0) {
			sendMessage(sender, getHelpProperty());
			return;
		}

		Player p = (Player) sender;

		Ticket ticket = getTicket(p, args[0]);

		if (ticket == null)
			return;

		if (ticket.isAssigned()) {
			sendMessage(p, "command.ticket.claim.already_assigned");
			return;
		}

		if (ticket.getStatus() == TicketStatus.CLOSED) {
			sendMessage(p, "command.ticket.claim.already_closed");
			return;
		}

		ticket.setAssignee(p.getUniqueId());

		sendMessage(p, "command.ticket.claim.success", ticket.getTicketId());

		if (ConfigManager.getConfig().getBoolean("notify.claim.moderators")) {
			String moderatorNotifyMessage =
					Messages.getString("notify.claim.moderators", p.getName(), ticket.getTicketId());

			notifyPermission(Permission.COMMAND_CLAIM_NOTIFY, moderatorNotifyMessage, p);
		}

		if (ConfigManager.getConfig().getBoolean("notify.claim.player")) {
			Player submitter = Bukkit.getPlayer(ticket.getSubmitter());

			if (submitter != null && submitter.hasPermission(Permission.COMMAND_CLAIM_NOTIFY.getStringPermission())) {
				sendMessage(submitter, "notify.claim.player", ticket.getTicketId(), p.getName());
			}
		}
	}
}
