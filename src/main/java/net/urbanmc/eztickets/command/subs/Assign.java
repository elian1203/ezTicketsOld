package net.urbanmc.eztickets.command.subs;

import net.urbanmc.eztickets.manager.ConfigManager;
import net.urbanmc.eztickets.manager.Messages;
import net.urbanmc.eztickets.object.Permission;
import net.urbanmc.eztickets.object.SubCommand;
import net.urbanmc.eztickets.object.Ticket;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Assign extends SubCommand {

	public Assign() {
		super("assign", Permission.COMMAND_ASSIGN, false, new String[0]);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (args.length < 2) {
			sendMessage(sender, getHelpProperty());
			return;
		}

		OfflinePlayer p = Bukkit.getOfflinePlayer(args[0]);

		if (p == null || !p.hasPlayedBefore() || p.getName() == null) {
			sendMessage(sender, "command.player_not_found");
			return;
		}

		Ticket ticket = getTicket(sender, args[1]);

		if (ticket == null)
			return;

		ticket.setAssignee(p.getUniqueId());

		sendMessage(sender, "command.ticket.assign.success", ticket.getTicketId(), p.getName());

		if (ConfigManager.getConfig().getBoolean("notify.assign.moderators")) {
			String moderatorNotifyMessage =
					Messages.getString("notify.assign.moderators", sender.getName(), ticket.getTicketId(), p.getName
							());

			notifyPermission(Permission.COMMAND_ASSIGN_NOTIFY, moderatorNotifyMessage, p);
		}

		if (p.isOnline()) {
			sendMessage((Player) p, "notify.assign.assignee", ticket.getTicketId(), sender.getName());
		}

		if (ConfigManager.getConfig().getBoolean("notify.assign.player")) {
			Player submitter = Bukkit.getPlayer(ticket.getSubmitter());

			if (submitter != null && submitter.hasPermission(Permission.COMMAND_ASSIGN_NOTIFY.getStringPermission())) {
				sendMessage(submitter, "notify.assign.player", ticket.getTicketId(), p.getName());
			}
		}
	}
}
