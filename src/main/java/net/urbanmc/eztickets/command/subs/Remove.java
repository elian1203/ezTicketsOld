package net.urbanmc.eztickets.command.subs;

import net.urbanmc.eztickets.manager.TicketManager;
import net.urbanmc.eztickets.object.Permission;
import net.urbanmc.eztickets.object.SubCommand;
import net.urbanmc.eztickets.object.Ticket;
import org.bukkit.command.CommandSender;

public class Remove extends SubCommand {

	public Remove() {
		super("remove", Permission.COMMAND_REMOVE, false, new String[]{"delete"});
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (args.length == 0) {
			sendMessage(sender, getHelpProperty());
			return;
		}

		Ticket ticket = getTicket(sender, args[0]);

		if (ticket == null)
			return;

		TicketManager.getInstance().removeTicket(ticket);
		sendMessage(sender, "command.ticket.remove.success", ticket.getTicketId());
	}
}
