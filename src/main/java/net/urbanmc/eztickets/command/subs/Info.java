package net.urbanmc.eztickets.command.subs;

import net.urbanmc.eztickets.manager.Messages;
import net.urbanmc.eztickets.object.Permission;
import net.urbanmc.eztickets.object.SubCommand;
import net.urbanmc.eztickets.object.Ticket;
import net.urbanmc.eztickets.object.TicketLocation;
import org.apache.commons.lang.WordUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;

public class Info extends SubCommand {

	public Info() {
		super("info", Permission.COMMAND_INFO, false, new String[]{"i"});
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (args.length == 0) {
			sendMessage(sender, getHelpProperty());
			return;
		}

		Ticket ticket = getTicket(sender, args[0]);

		if (ticket == null)
			return;

		if (!sender.hasPermission(Permission.COMMAND_INFO_OTHERS.getStringPermission())) {
			Player p = (Player) sender;

			if (p.getUniqueId().equals(ticket.getSubmitter())) {
				sendInfo(p, ticket);
				return;
			} else {
				sendMessage(p, "command.ticket.info.not_yours");
				return;
			}
		}

		sendInfo(sender, ticket);
	}

	private void sendInfo(CommandSender sender, Ticket ticket) {
		int ticketId = ticket.getTicketId();

		String[] names = getNames(ticket);

		String submitter = names[0], assignee =
				names[1] != null ? names[1] : Messages.getString("command.ticket.info.message.not_assigned");

		String status = WordUtils.capitalizeFully(ticket.getStatus().name()), timeSubmitted =
				formatTime(ticket.getTimeSubmitted()), location = formatLocation(ticket.getLocation()), detail =
				ticket.getDetail();

		sendMessage(
				sender,
				"command.ticket.info.message",
				ticketId,
				submitter,
				status,
				assignee,
				timeSubmitted,
				location,
				detail);
	}

	private String formatLocation(TicketLocation loc) {
		String world = loc.getWorld();
		double x = loc.getX(), y = loc.getY(), z = loc.getZ();

		DecimalFormat df = new DecimalFormat("#.##");

		return String.format("%s, %s, %s, %s", world, df.format(x), df.format(y), df.format(z));
	}
}
