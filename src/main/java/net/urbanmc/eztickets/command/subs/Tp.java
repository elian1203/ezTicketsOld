package net.urbanmc.eztickets.command.subs;

import net.urbanmc.eztickets.object.Permission;
import net.urbanmc.eztickets.object.SubCommand;
import net.urbanmc.eztickets.object.Ticket;
import net.urbanmc.eztickets.object.TicketLocation;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Tp extends SubCommand {

	public Tp() {
		super("tp", Permission.COMMAND_TP, true, new String[]{"teleport"});
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (args.length == 0) {
			sendMessage(sender, getHelpProperty());
			return;
		}

		Ticket ticket = getTicket(sender, args[0]);

		if (ticket == null)
			return;

		Player p = (Player) sender;

		if (!p.hasPermission(Permission.COMMAND_TP_NOT_CLAIMED.getStringPermission())) {
			boolean isAssignee = ticket.getAssignee() != null && ticket.getAssignee().equals(p.getUniqueId());

			if (!isAssignee) {
				sendMessage(p, "command.ticket_not_assigned");
				return;
			}
		}

		TicketLocation loc = ticket.getLocation();

		if (!loc.isValidLocation()) {
			sendMessage(p, "command.ticket.tp.invalid");
			return;
		}

		p.teleport(loc.getBukkitLocation());
		sendMessage(p, "command.ticket.tp.success", ticket.getTicketId());
	}
}
