package net.urbanmc.eztickets.command.subs;

import net.urbanmc.eztickets.manager.ConfigManager;
import net.urbanmc.eztickets.object.*;
import org.bukkit.command.CommandSender;

import java.util.List;

public class Messages extends SubCommand {

	public Messages() {
		super("messages", Permission.COMMAND_MESSAGES, false, new String[0]);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (args.length == 0) {
			sendMessage(sender, getHelpProperty());
			return;
		}

		Ticket ticket = getTicket(sender, args[0]);

		if (ticket == null)
			return;

		List<ChatMessage> messages = ticket.getRecentMessages();

		if (messages.isEmpty()) {
			sendMessage(sender, "command.ticket.messages.none");
			return;
		}

		if (args.length > 1 && !isInt(args[1])) {
			if (args[1].equalsIgnoreCase("all")) {
				sendChatMessages(sender, messages);
			} else {
				sendMessage(sender, getHelpProperty());
			}

			return;
		}

		int amountPerPage = ConfigManager.getConfig().getInt("amount-per-page");

		PaginalList<ChatMessage> paginalList = new PaginalList<>(messages, amountPerPage);

		int page = args.length > 1 ? Integer.parseInt(args[1]) : 1, amountOfPages = paginalList.getAmountOfPages();

		if (page > amountOfPages) {
			sendMessage(sender, "command.ticket.messages.max", amountOfPages);
			return;
		}

		List<ChatMessage> partition = paginalList.getPage(page);

		sendChatMessages(sender, partition);
		sendMessage(sender, "list_message_format", page, amountOfPages);
	}

	private void sendChatMessages(CommandSender sender, List<ChatMessage> messages) {
		for (ChatMessage message : messages) {
			sender.sendMessage(message.format());
		}
	}
};
