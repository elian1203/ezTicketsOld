package net.urbanmc.eztickets.command.subs;

import com.google.common.collect.Lists;
import net.urbanmc.eztickets.manager.ConfigManager;
import net.urbanmc.eztickets.manager.Messages;
import net.urbanmc.eztickets.manager.TicketManager;
import net.urbanmc.eztickets.object.PaginalList;
import net.urbanmc.eztickets.object.Permission;
import net.urbanmc.eztickets.object.SubCommand;
import net.urbanmc.eztickets.object.Ticket;
import net.urbanmc.eztickets.object.Ticket.TicketStatus;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.stream.Collectors;

public class ListSub extends SubCommand {

	public ListSub() {
		super("list", Permission.COMMAND_LIST, false, new String[0]);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		if (args.length == 0) {
			sendMessage(sender, getHelpProperty());
			return;
		}

		int amountPerPage = ConfigManager.getConfig().getInt("amount-per-page"), page = 1;

		if (args.length > 1 && isInt(args[1])) {
			page = Integer.parseInt(args[1]);
		}

		String headerType;
		PaginalList<String> paginalList = null;

		List<Ticket> tickets = Lists.reverse(TicketManager.getInstance().getTickets());
		List<String> formatted;

		switch (args[0].toLowerCase()) {
			case "open":
				if (!sender.hasPermission(Permission.COMMAND_LIST_OPEN.getStringPermission())) {
					sendMessage(sender, "command.ticket.list.no_permission");
					return;
				}

				headerType = Messages.getString("command.ticket.list.header.open");

				formatted = tickets.stream().filter(t -> t.getStatus() == TicketStatus.OPEN).map(this::formatTicket)
						.collect(Collectors.toList());
				paginalList = new PaginalList<>(formatted, amountPerPage);

				break;
			case "assigned":
				if (!sender.hasPermission(Permission.COMMAND_LIST_CLAIMED.getStringPermission())) {
					sendMessage(sender, "command.ticket.list.no_permission");
					return;
				}

				headerType = Messages.getString("command.ticket.list.header.assigned");

				formatted = tickets.stream().filter(t -> t.getAssignee() != null).map(this::formatTicket)
						.collect(Collectors.toList());
				paginalList = new PaginalList<>(formatted, amountPerPage);

				break;
			case "closed":
				if (!sender.hasPermission(Permission.COMMAND_LIST_CLOSED.getStringPermission())) {
					sendMessage(sender, "command.ticket.list.no_permission");
					return;
				}

				headerType = Messages.getString("command.ticket.list.header.closed");

				formatted = tickets.stream().filter(t -> t.getStatus() == TicketStatus.CLOSED).map(this::formatTicket)
						.collect(Collectors.toList());
				paginalList = new PaginalList<>(formatted, amountPerPage);
				break;
			case "all":
				if (!sender.hasPermission(Permission.COMMAND_LIST_ALL.getStringPermission())) {
					sendMessage(sender, "command.ticket.list.no_permission");
					return;
				}

				headerType = Messages.getString("command.ticket.list.header.all");

				formatted = tickets.stream().map(this::formatTicket).collect(Collectors.toList());
				paginalList = new PaginalList<>(formatted, amountPerPage);

				break;
			case "mine":
				if (!sender.hasPermission(Permission.COMMAND_LIST_MINE.getStringPermission())) {
					sendMessage(sender, "command.ticket.list.no_permission");
					return;
				}

				if (!(sender instanceof Player)) {
					sendMessage(sender, "command.player_only");
					return;
				}

				headerType = Messages.getString("command.ticket.list.header.mine");

				tickets = TicketManager.getInstance().getTicketsForPlayer(((Player) sender).getUniqueId());

				formatted = tickets.stream().map(this::formatTicket).collect(Collectors.toList());
				paginalList = new PaginalList<>(formatted, amountPerPage);

				break;
			case "player":
				if (!sender.hasPermission(Permission.COMMAND_LIST_PLAYER.getStringPermission())) {
					sendMessage(sender, "command.ticket.list.no_permission");
					return;
				}

				if (args.length == 1) {
					sendMessage(sender, "command.specify_player");
					return;
				}

				OfflinePlayer p = Bukkit.getOfflinePlayer(args[1]);

				if (p == null || p.getName() == null) {
					sendMessage(sender, "command.player_not_found");
					return;
				}

				headerType = Messages.getString("command.ticket.list.header.player", p.getName());

				tickets = TicketManager.getInstance().getTicketsForPlayer(p.getUniqueId());

				if (tickets.isEmpty())
					break;

				formatted = tickets.stream().map(this::formatTicket).collect(Collectors.toList());
				formatted = Lists.reverse(formatted);

				paginalList = new PaginalList<>(formatted, amountPerPage);

				if (args.length > 2 && isInt(args[2])) {
					page = Integer.parseInt(args[2]);
				}

				break;
			default:
				sendMessage(sender, getHelpProperty());
				return;
		}

		if (paginalList == null || paginalList.getAmountOfPages() == 0 || paginalList.getPage(1).isEmpty()) {
			sendMessage(sender, "command.ticket.list.none");
			return;
		}

		int amountOfPages = paginalList.getAmountOfPages();

		if (amountOfPages < page) {
			sendMessage(sender, "command.ticket.list.max", amountOfPages);
			return;
		}

		sendMessage(sender, "command.ticket.list.header", headerType);
		paginalList.getPage(page).forEach(sender::sendMessage);
		sendMessage(sender, "list_message_format", page, amountOfPages);
	}

	private String formatTicket(Ticket ticket) {
		String status = Messages.getString("command.ticket.list.header." + ticket.getStatus().name().toLowerCase());

		return Messages.getString(
				"command.ticket.list.entry",
				ticket.getTicketId(),
				getNames(ticket)[0],
				formatTime(ticket.getTimeSubmitted()),
				status,
				ticket.getDetail());
	}
}
