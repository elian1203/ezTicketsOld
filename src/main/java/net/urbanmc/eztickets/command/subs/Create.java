package net.urbanmc.eztickets.command.subs;

import net.urbanmc.eztickets.manager.ConfigManager;
import net.urbanmc.eztickets.manager.Messages;
import net.urbanmc.eztickets.manager.TicketManager;
import net.urbanmc.eztickets.object.*;
import org.apache.commons.lang.StringUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public class Create extends SubCommand {

	public Create() {
		super("create", Permission.COMMAND_CREATE, true, new String[]{"new", "open"});
	}

	@Override
	public synchronized void execute(CommandSender sender, String[] args) {
		if (args.length == 0) {
			sendMessage(sender, getHelpProperty());
			return;
		}

		Player p = (Player) sender;

		int configMax = ConfigManager.getConfig().getInt("max-open-per-player");

		if (TicketManager.getInstance().getOpenTicketsForPlayer(p.getUniqueId()) >= configMax) {
			sendMessage(p, "command.ticket.create.max_already_reached");
			return;
		}

		int ticketId = TicketManager.getInstance().getNewTicketId();
		UUID submitter = p.getUniqueId();
		long timeSubmitted = System.currentTimeMillis();
		TicketLocation location = new TicketLocation(p.getLocation());
		String detail = StringUtils.join(args, " ");
		List<ChatMessage> messages = TicketManager.getInstance().getRecentMessages();

		Ticket ticket = new Ticket(ticketId, submitter, null, timeSubmitted, location, detail, messages);

		TicketManager.getInstance().addTicket(ticket);

		sendMessage(p, "command.ticket.create.success", ticket.getTicketId());

		if (ConfigManager.getConfig().getBoolean("notify.create.moderators")) {
			String moderatorNotifyMessage =
					Messages.getString("notify.create.moderators", p.getName(), ticket.getTicketId());
			notifyPermission(Permission.COMMAND_CREATE_NOTIFY, moderatorNotifyMessage, p);
		}
	}
}
