package net.urbanmc.eztickets.command.subs;

import net.urbanmc.eztickets.object.Permission;
import net.urbanmc.eztickets.object.SubCommand;
import net.urbanmc.eztickets.object.Ticket;
import org.apache.commons.lang.StringUtils;
import org.bukkit.command.CommandSender;

import java.util.Arrays;

public class AddDetail extends SubCommand {

	public AddDetail() {
		super("adddetail", Permission.COMMAND_ADD_DETAIL, false, new String[0]);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (args.length < 2) {
			sendMessage(sender, getHelpProperty());
			return;
		}

		Ticket ticket = getTicket(sender, args[0]);

		if (ticket == null)
			return;

		String extraDetail = StringUtils.join(Arrays.copyOfRange(args, 1, args.length), " ");

		ticket.addDetail(extraDetail);

		sendMessage(sender, "command.ticket.adddetail.success", ticket.getTicketId());
	}
}
