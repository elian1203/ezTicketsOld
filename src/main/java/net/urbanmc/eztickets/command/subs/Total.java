package net.urbanmc.eztickets.command.subs;

import net.urbanmc.eztickets.manager.TicketManager;
import net.urbanmc.eztickets.object.Permission;
import net.urbanmc.eztickets.object.SubCommand;
import net.urbanmc.eztickets.object.Ticket;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class Total extends SubCommand {

	public Total() {
		super("total", Permission.COMMAND_TOTAL, false, new String[0]);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		OfflinePlayer p;

		if (args.length == 0 && !(sender instanceof Player)) {
			sendMessage(sender, "command.specify_player");
			return;
		} else if (args.length > 0 && sender.hasPermission(Permission.COMMAND_TOTAL_OTHERS.getStringPermission())) {
			p = Bukkit.getOfflinePlayer(args[0]);

			if (p == null || !p.hasPlayedBefore() || p.getName() == null) {
				sendMessage(sender, "command.player_not_found");
				return;
			}
		} else {
			p = (OfflinePlayer) sender;
		}

		List<Ticket> tickets = TicketManager.getInstance().getTicketsForPlayer(p.getUniqueId());

		int amountOpen = 0, amountAssigned = 0, amountClosed = 0;

		for (Ticket ticket : tickets) {
			switch (ticket.getStatus()) {
				case OPEN:
					amountOpen++;
					break;
				case ASSIGNED:
					amountAssigned++;
					break;
				case CLOSED:
					amountClosed++;
					break;
			}
		}

		if (p.getName().equalsIgnoreCase(sender.getName())) {
			sendMessage(sender, "command.ticket.total.message.you", amountOpen, amountAssigned, amountClosed);
		} else {
			sendMessage(sender, "command.ticket.total.message", p.getName(), amountOpen, amountAssigned, amountClosed);
		}
	}
}
