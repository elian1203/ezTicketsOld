package net.urbanmc.eztickets.command.subs;

import net.urbanmc.eztickets.manager.ConfigManager;
import net.urbanmc.eztickets.manager.Messages;
import net.urbanmc.eztickets.object.Permission;
import net.urbanmc.eztickets.object.SubCommand;
import net.urbanmc.eztickets.object.Ticket;
import net.urbanmc.eztickets.object.Ticket.TicketStatus;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Close extends SubCommand {

	public Close() {
		super("close", Permission.COMMAND_CLOSE, false, new String[0]);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (args.length == 0) {
			sendMessage(sender, getHelpProperty());
			return;
		}

		Ticket ticket = getTicket(sender, args[0]);

		if (ticket == null)
			return;

		if (ticket.getStatus() == TicketStatus.CLOSED) {
			sendMessage(sender, "command.ticket.close.already_closed");
			return;
		}

		if (!sender.hasPermission(Permission.COMMAND_CLOSE_NOT_CLAIMED.getStringPermission())) {
			Player p = (Player) sender;

			boolean isAssignee = ticket.isAssigned() && ticket.getAssignee().equals(p.getUniqueId());

			if (!isAssignee) {
				sendMessage(sender, "command.ticket_not_assigned");
				return;
			}
		}

		ticket.close();

		sendMessage(sender, "command.ticket.close.success", ticket.getTicketId());

		if (ConfigManager.getConfig().getBoolean("notify.close.moderators")) {
			String moderatorNotifyMessage =
					Messages.getString("notify.close.moderators", sender.getName(), ticket.getTicketId());

			notifyPermission(
					Permission.COMMAND_CLOSE_NOTIFY,
					moderatorNotifyMessage,
					sender instanceof Player ? (Player) sender : null);
		}

		if (ConfigManager.getConfig().getBoolean("notify.close.player")) {
			Player submitter = Bukkit.getPlayer(ticket.getSubmitter());

			if (submitter != null && submitter.hasPermission(Permission.COMMAND_CLOSE_NOTIFY.getStringPermission())) {
				sendMessage(submitter, "notify.close.player", ticket.getTicketId(), sender.getName());
			}
		}
	}
}
