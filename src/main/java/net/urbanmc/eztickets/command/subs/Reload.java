package net.urbanmc.eztickets.command.subs;

import net.urbanmc.eztickets.manager.ConfigManager;
import net.urbanmc.eztickets.manager.Messages;
import net.urbanmc.eztickets.manager.TicketManager;
import net.urbanmc.eztickets.object.Permission;
import net.urbanmc.eztickets.object.SubCommand;
import org.bukkit.command.CommandSender;

public class Reload extends SubCommand {

	public Reload() {
		super("reload", Permission.COMMAND_RELOAD, false, new String[0]);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		ConfigManager.getInstance().reloadConfiguration();
		TicketManager.getInstance().reloadChatMessages();
		Messages.getInstance().reload();

		sendMessage(sender, "command.ticket.reload.success");
	}
}
