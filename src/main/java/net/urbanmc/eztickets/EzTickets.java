package net.urbanmc.eztickets;

import net.urbanmc.eztickets.command.TicketCommand;
import net.urbanmc.eztickets.listener.ChatListener;
import net.urbanmc.eztickets.listener.JoinListener;
import org.bstats.Metrics;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class EzTickets extends JavaPlugin {

	public void onEnable() {
		registerCommand();
		registerListeners();
		registerMetrics();
	}

	private void registerCommand() {
		getCommand("ticket").setExecutor(new TicketCommand());
	}

	private void registerListeners() {
		PluginManager pm = getServer().getPluginManager();

		pm.registerEvents(new ChatListener(), this);
		pm.registerEvents(new JoinListener(this), this);
	}

	private void registerMetrics() {
		new Metrics(this);
	}
}
