package net.urbanmc.eztickets.object;

public enum Permission {

	COMMAND_BASE("command.ticket"),
	COMMAND_ADD_DETAIL("command.ticket.adddetail"),
	COMMAND_ASSIGN("command.ticket.assign"),
	COMMAND_ASSIGN_NOTIFY("command.ticket.assign.notify"),
	COMMAND_CLAIM("command.ticket.claim"),
	COMMAND_CLAIM_NOTIFY("command.ticket.claim.notify"),
	COMMAND_CLOSE("command.ticket.close"),
	COMMAND_CLOSE_NOT_CLAIMED("command.ticket.close.not-claimed"),
	COMMAND_CLOSE_NOTIFY("command.ticket.close.notify"),
	COMMAND_CREATE("command.ticket.create"),
	COMMAND_CREATE_NOTIFY("command.ticket.create.notify"),
	COMMAND_INFO("command.ticket.info"),
	COMMAND_INFO_OTHERS("command.ticket.info.others"),
	COMMAND_LIST("command.ticket.list"),
	COMMAND_LIST_OPEN("command.ticket.list.open"),
	COMMAND_LIST_CLAIMED("command.ticket.list.claimed"),
	COMMAND_LIST_CLOSED("command.ticket.list.closed"),
	COMMAND_LIST_ALL("command.ticket.list.all"),
	COMMAND_LIST_MINE("command.ticket.list.mine"),
	COMMAND_LIST_PLAYER("command.ticket.list.player"),
	COMMAND_MESSAGES("command.ticket.messages"),
	COMMAND_RELOAD("command.ticket.reload"),
	COMMAND_REMOVE("command.ticket.remove"),
	COMMAND_TOTAL("command.ticket.total"),
	COMMAND_TOTAL_OTHERS("command.ticket.total.others"),
	COMMAND_TP("command.ticket.tp"),
	COMMAND_TP_NOT_CLAIMED("command.ticket.tp.not-claimed"),
	JOIN_NOTIFY("notify.join"),
	JOIN_NOTIFY_MODERATOR("notify.join.moderator");

	private String bukkitPermission;

	Permission(String bukkitPermission) {
		this.bukkitPermission = bukkitPermission;
	}

	public String getStringPermission() {
		return "eztickets." + bukkitPermission;
	}
}
