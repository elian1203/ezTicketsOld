package net.urbanmc.eztickets.object;

import net.urbanmc.eztickets.manager.Messages;

public class ChatMessage {

	private String sender;
	private String message;

	public ChatMessage(String sender, String message) {
		this.sender = sender;
		this.message = message;
	}

	public String getSender() {
		return sender;
	}

	public String getMessage() {
		return message;
	}

	public String format() {
		return Messages.getString("chat_message_format", sender, message);
	}
}
