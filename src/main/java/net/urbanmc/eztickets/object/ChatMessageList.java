package net.urbanmc.eztickets.object;

import net.urbanmc.eztickets.manager.ConfigManager;

import java.util.ArrayList;

public class ChatMessageList extends ArrayList<ChatMessage> {

	private static int maxChatMessages;

	public ChatMessageList() {
		super();
		updateMaxChatMessages();
	}

	public static void updateMaxChatMessages() {
		maxChatMessages = ConfigManager.getConfig().getInt("chat-messages-logged");
	}

	@Override
	public boolean add(ChatMessage message) {
		if (size() == maxChatMessages) {
			remove(0);
		}

		return super.add(message);
	}
}
