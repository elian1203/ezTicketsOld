package net.urbanmc.eztickets.object;

import net.urbanmc.eztickets.NameFetcher;
import net.urbanmc.eztickets.manager.Messages;
import net.urbanmc.eztickets.manager.TicketManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;

public abstract class SubCommand {

	private String subCommand;
	private Permission permission;
	private boolean playerOnly;
	private String[] aliases;

	public SubCommand(String subCommand, Permission permission, boolean playerOnly, String[] aliases) {
		this.subCommand = subCommand;
		this.permission = permission;
		this.playerOnly = playerOnly;
		this.aliases = aliases;
	}

	public String getSubCommand() {
		return subCommand;
	}

	public Permission getPermission() {
		return permission;
	}

	public boolean isPlayerOnly() {
		return playerOnly;
	}

	public String[] getAliases() {
		return aliases;
	}

	public String getHelpProperty() {
		return "command.ticket." + subCommand + ".help";
	}

	public abstract void execute(CommandSender sender, String[] args);

	protected void sendMessage(CommandSender sender, String property, Object... args) {
		String message = Messages.getString(property, args);

		if (message.isEmpty())
			return;

		if (sender instanceof Player) {
			sender.sendMessage(message);
		} else {
			sender.sendMessage(ChatColor.stripColor(message));
		}
	}

	protected boolean isInt(String number) {
		try {
			Integer.parseInt(number);
			return true;
		} catch (NumberFormatException ex) {
			return false;
		}
	}

	protected Ticket getTicket(CommandSender sender, String arg) {
		if (!isInt(arg)) {
			sendMessage(sender, "command.invalid_id");
			return null;
		}

		int ticketId = Integer.parseInt(arg);

		Ticket ticket = TicketManager.getInstance().getTicketById(ticketId);

		if (ticket == null) {
			sendMessage(sender, "command.ticket_not_found");
		}

		return ticket;
	}

	protected String formatTime(long millis) {
		Calendar cal = new GregorianCalendar();

		cal.setTime(new Date(millis));

		int month = cal.get(Calendar.MONTH) + 1, day = cal.get(Calendar.DAY_OF_MONTH), year = cal.get(Calendar.YEAR),
				hour = cal.get(Calendar.HOUR_OF_DAY), minute = cal.get(Calendar.MINUTE);

		String stringHour = hour < 10 ? "0" + hour : Integer.toString(hour), stringMinute =
				minute < 10 ? "0" + minute : Integer.toString(minute);

		String timeZone = cal.getTimeZone().getDisplayName(false, TimeZone.SHORT);

		return Messages
				.getString("time_format", month, day, Integer.toString(year), stringHour, stringMinute, timeZone);
	}

	protected String[] getNames(Ticket ticket) {
		OfflinePlayer submitter = Bukkit.getOfflinePlayer(ticket.getSubmitter()), assignee =
				ticket.getAssignee() == null ? null : Bukkit.getOfflinePlayer(ticket.getAssignee());

		List<UUID> uuids = new ArrayList<>();

		String[] names = new String[2];

		if (submitter.getName() == null) {
			uuids.add(ticket.getSubmitter());
		} else {
			names[0] = submitter.getName();
		}

		if (assignee != null && assignee.getName() != null) {
			names[1] = assignee.getName();
		} else if (assignee != null && assignee.getName() == null) {
			uuids.add(ticket.getAssignee());
		}

		if (uuids.isEmpty())
			return names;

		Map<UUID, String> map = fetchNames(uuids);

		if (map.containsKey(ticket.getSubmitter())) {
			names[0] = map.get(ticket.getSubmitter());
		}

		if (ticket.getAssignee() != null && map.containsKey(ticket.getAssignee())) {
			names[1] = map.get(ticket.getAssignee());
		}

		return names;
	}

	protected void notifyPermission(Permission permission, String message, OfflinePlayer exclude) {
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (p != exclude && p.hasPermission(permission.getStringPermission())) {
				p.sendMessage(message);
			}
		}
	}

	private Map<UUID, String> fetchNames(List<UUID> uuids) {
		try {
			return new NameFetcher(uuids).call();
		} catch (Exception ex) {
			return new HashMap<>();
		}
	}
}
