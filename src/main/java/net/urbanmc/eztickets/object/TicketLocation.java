package net.urbanmc.eztickets.object;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

public class TicketLocation {

	private String world;
	private double x, y, z;
	private float yaw, pitch;

	public TicketLocation(Location loc) {
		this(loc.getWorld().getName(), loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
	}

	public TicketLocation(String world, double x, double y, double z, float yaw, float pitch) {
		this.world = world;
		this.x = x;
		this.y = y;
		this.z = z;
		this.yaw = yaw;
		this.pitch = pitch;
	}

	public String getWorld() {
		return world;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getZ() {
		return z;
	}

	public boolean isValidLocation() {
		return getBukkitWorld() != null;
	}

	public Location getBukkitLocation() {
		return new Location(getBukkitWorld(), x, y, z, yaw, pitch);
	}

	private World getBukkitWorld() {
		return Bukkit.getWorld(world);
	}
}
