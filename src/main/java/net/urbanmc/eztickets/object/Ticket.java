package net.urbanmc.eztickets.object;

import net.urbanmc.eztickets.manager.ConfigManager;
import net.urbanmc.eztickets.manager.TicketManager;

import java.util.List;
import java.util.UUID;

public class Ticket {

	private int ticketId;
	private UUID submitter, assignee;
	private long timeSubmitted;
	private TicketLocation location;
	private String detail;
	private List<ChatMessage> recentMessages;
	private TicketStatus status;

	public Ticket(int ticketId, UUID submitter, UUID assignee, long timeSubmitted, TicketLocation location,
	              String detail, List<ChatMessage> recentMessages) {
		this(ticketId, submitter, assignee, timeSubmitted, location, detail, recentMessages, TicketStatus.OPEN);
	}

	public Ticket(int ticketId, UUID submitter, UUID assignee, long timeSubmitted, TicketLocation location,
	              String detail, List<ChatMessage> recentMessages, TicketStatus status) {
		this.ticketId = ticketId;
		this.submitter = submitter;
		this.assignee = assignee;
		this.timeSubmitted = timeSubmitted;
		this.location = location;
		this.detail = detail;
		this.recentMessages = recentMessages;
		this.status = status;
	}

	public int getTicketId() {
		return ticketId;
	}

	public UUID getSubmitter() {
		return submitter;
	}

	public UUID getAssignee() {
		return assignee;
	}

	public void setAssignee(UUID assignee) {
		this.assignee = assignee;
		status = TicketStatus.ASSIGNED;

		TicketManager.getInstance().saveTickets();
	}

	public boolean isAssigned() {
		return getAssignee() != null && status != TicketStatus.CLOSED;
	}

	public long getTimeSubmitted() {
		return timeSubmitted;
	}

	public TicketLocation getLocation() {
		return location;
	}

	public String getDetail() {
		return detail;
	}

	public void addDetail(String detail) {
		this.detail += "\n" + detail;
		TicketManager.getInstance().saveTickets();
	}

	public List<ChatMessage> getRecentMessages() {
		return recentMessages;
	}

	public TicketStatus getStatus() {
		return status;
	}

	public void close() {
		if (ConfigManager.getConfig().getBoolean("delete-closed")) {
			TicketManager.getInstance().removeTicket(this);
		} else {
			status = TicketStatus.CLOSED;
		}

		TicketManager.getInstance().saveTickets();
	}

	public enum TicketStatus {
		OPEN, ASSIGNED, CLOSED
	}
}
